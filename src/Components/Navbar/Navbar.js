import React from 'react'
import { BsSpotify } from "react-icons/bs";

function Navbar() {
    return (
        <>
            <nav class="navbar navbar-expand-lg fixed-top bg-black text-white ">
                <div class="container-fluid">
                    <a class="navbar-brand" className='text-white text-decoration-none ms-3 fs-1' href="google">
                        <BsSpotify /> Spotify
                    </a>
                    <button class="navbar-toggler rounded-pill bg-light btn btn-light"
                        // type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto my-2">
                            <li class="nav-item">
                                <button class="btn text-white rounded-pill" type="submit">Sign up</button>
                            </li>
                            <li class="nav-item">
                                <button class="btn btn-light rounded-pill" type="submit">Login</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}

export default Navbar