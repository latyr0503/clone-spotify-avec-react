import React from 'react'
import './Sidebar.css'
import { BsFillHouseDoorFill, BsGearWide, BsGlobe, BsFillChatSquareHeartFill, BsSearch, BsFillPlusSquareFill, BsJournalBookmark } from "react-icons/bs";
// import Dashboard from '../Dashboard/Dashboard';
import { Rectangle } from '../Data/Data'
import { Rectangle2 } from '../Data/Data'

function Sidebar() {
    return (
        <>
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-lg-2 text-start min-vh-100 bg-black text-white d-lg-flex d-none'>
                        <ul className='list-unstyled SideBar'>
                            <li>
                                <a href='google' className='nav-link p-2'>
                                    <BsFillHouseDoorFill />
                                    <span className='m-3'>
                                        Home
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href='google' className='nav-link p-2'>
                                    <BsSearch />
                                    <span className='m-3 '>
                                        Search
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href='google' className='nav-link p-2'>
                                    <BsJournalBookmark />
                                    <span className='m-3'>
                                        Your Library
                                    </span>
                                </a>
                            </li>
                            <br />
                            <li>
                                <a href='google' className='nav-link p-2'>
                                    <BsFillPlusSquareFill />
                                    <span className='m-3 '>
                                        Create Playlist
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href='google' className='nav-link p-2'>
                                    <BsFillChatSquareHeartFill />
                                    <span className='m-3 '>
                                        Liked Songs
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div className='px-3 fixed-bottom'>
                            <ul className='list-unstyled list-inline SideFooter text-secondary'>
                                <li class="list-inline-item">Legal</li>
                                <li class="list-inline-item">Privacy Center</li>
                                <li class="list-inline-item">Privacy Policy</li>
                                <br />
                                <li class="list-inline-item">Cookies</li>
                                <li class="list-inline-item">About</li>
                                <li class="list-inline-item">Ads</li>
                                <button class="my-3 btn px-3 btn-outline-light d-flex align-items-center rounded-pill">
                                    <BsGlobe className='mx-2' /> English
                                </button>
                            </ul>
                        </div>

                    </div>
                    <nav class="navbar fixed-bottom bg-light d-lg-none SidebarResponsive">
                        <div class="container-fluid ">
                            <ul className='list-unstyled  mx-auto fs-2 text-dark d-flex'>
                                <li>
                                    <a href='google' className='nav-link'>
                                        <BsFillHouseDoorFill />
                                    </a>
                                </li>
                                <li>
                                    <a href='google' className='nav-link'>
                                        <BsSearch />
                                    </a>
                                </li>
                                <li>
                                    <a href='google' className='nav-link'>
                                        <BsJournalBookmark />
                                    </a>
                                </li>

                                <li>
                                    <a href='google' className='nav-link'>
                                        <BsFillPlusSquareFill />
                                    </a>
                                </li>
                                <li>
                                    <a href='google' className='nav-link'>
                                        <BsFillChatSquareHeartFill />


                                    </a>
                                </li>
                                <li>
                                    <a href='google' className='nav-link' type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling">
                                        <BsGearWide />
                                    </a>
                                </li>

                            </ul>
                            <div class="offcanvas offcanvas-end" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
                                <div class="offcanvas-header">
                                    <h5 class="offcanvas-title" id="offcanvasScrollingLabel">
                                        <BsGearWide />
                                    </h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                </div>
                                <div class="offcanvas-body">
                                    <div >
                                        <ul className='list-unstyled text-end'>
                                            <li>
                                                <a href='google' className='text-decoration-none text-black'>Legal</a>
                                            </li>
                                            <li>
                                                <a href='google' className='text-decoration-none text-black'>Privacy Center</a>
                                            </li>
                                            <li >
                                                <a href='google' className='text-decoration-none text-black' >Privacy Policy</a>
                                            </li>
                                            <li>
                                                <a href='google' className='text-decoration-none text-black'>Cookies</a>
                                            </li>
                                            <li>
                                                <a href='google' className='text-decoration-none text-black' >About</a>
                                            </li>
                                            <li >
                                                <a href='google' className='text-decoration-none text-black' >Ads</a>
                                            </li>
                                            <button class="my-3 btn ms-auto px-3 btn-dark d-flex align-items-center rounded-pill">
                                                <BsGlobe className='mx-2' /> English
                                            </button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div className='col-lg-10 col-md-12 bg-dark'>
                        {/* c'est ici que tu dois mettre ton dashboard */}
                        <div className="row mt-5">
                            <div className='d-flex px-5 mt-5 flex-row flex-md-row justify-content-around justify-content-md-around'>
                                <div className="col-md-6">
                                    <h5 className='text-start text-white'>Focus</h5>
                                </div>
                                <div className="col-md-6">
                                    <h5 className='text-end text-light'>Show all</h5>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className='d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-md-center align-items-md-center'>
                                {Rectangle.map((item) => {
                                    return (
                                        <div className="col text-center mt-5">
                                            <img src={item.img} alt="" className='img-fluid mx-auto w-100 h-75' />
                                            <p className='fs-5 fw-semibold text-center text-start'>{item.titre}</p>
                                            <p className='text-white text-center'>{item.text}</p>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>

                        <div className="col-12 my-2">
                            <h1 className='fs-1 fw-bold text-start text-light'>Spotify Playlist</h1>
                        </div>
                        <div className="row">
                            <div className='d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-md-center align-items-md-center'>
                                {Rectangle2.map((item) => {
                                    return (
                                        <div className="col text-center">
                                            <img src={item.img} alt="" className='img-fluid mx-auto w-100 h-75' />
                                            <p className='fs-5 fw-semibold text-center text-start'>{item.titre}</p>
                                            <p className='text-white text-center'>{item.text}</p>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                        {/* fin de ton dashboard */}
                    </div>
                </div>

            </div>
        </>
    )
}

export default Sidebar