import Rectagle1 from "../../Images/Rectangle/Rectangle1.png";
import Rectagle2 from "../../Images/Rectangle/Rectangle2.png";
import Rectagle3 from "../../Images/Rectangle/Rectangle3.png";
import Rectagle4 from "../../Images/Rectangle/Rectangle4.png";
import Rectagle5 from "../../Images/Rectangle/Rectangle5.png";

import Rectagle6 from "../../Images/Rectangle/Rectangle6.png";
import Rectagle7 from "../../Images/Rectangle/Rectangle7.png";
import Rectagle8 from "../../Images/Rectangle/Rectangle8.png";
import Rectagle9 from "../../Images/Rectangle/Rectangle9.png";
import Rectagle10 from "../../Images/Rectangle/Rectangle10.png";

export const Rectangle = [
    {
        img: Rectagle1,
        titre : "Peaceful Piano",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle2,
        titre : "Deep Focus",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle3,
        titre : "Instrumental Study",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle4,
        titre : "Focus Flow",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle5,
        titre : "Beats to think to",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
];

export const Rectangle2 = [
    {
        img: Rectagle6,
        titre : "Today's to Hits",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle7,
        titre : "Rap Calviar",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle8,
        titre : "All Out 2010s",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle9,
        titre : "Rock Classics",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
        img: Rectagle10,
        titre : "Chill Hits",
        text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit."
    },
];
