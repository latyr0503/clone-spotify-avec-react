import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Sidebar from './Components/Sidebar/Sidebar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
// import Dashboard from './Components/Dashboard/Dashboard';

function App() {
  return (
    <div className="App">
      <Navbar /> 
      <Sidebar /> 
      {/* <Dashboard /> */}
    </div>
  );
}

export default App;
